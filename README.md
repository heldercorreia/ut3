﻿# Preparing strings for internationalisation

Each string which is going to be internationalised must be declared to be a translatable string. This is done by enclosing the string in special delimiters. These delimiters can be defined completely free. To get reasonable results, you should usually use characters or strings which will not appear in the other text in the rest of the page.

For each translatable string there is always a left and a right delimiter. It is not necessary, though, that both delimiters are actually different from each other. Therefore, depending on the contents of your static pages, the following three could be perfectly valid examples of defining the strings to be internationalised by using delimiters:

    $$This is a translatable string.$$
    [This is a translatable string.]
    <i18n>This is a translatable string.</i18n>

# Using hints

Hints are a way of giving the translators additional information about the context of the string to be internationalised. Using hints is completely optional.

Hints are enclosed in their own delimiters. It is necessary and important that these hint delimiters are different from the string delimiters as defined above. The only other constraint is that hints *must* always start directly after the left delimiter of the translatable string. Some examples are:

    $$@@using a hint now@@another translatable string
    [((using a hint now))another translatable string]
    <i18n><hint>using a hint now</hint>another translatable string</i18n>
