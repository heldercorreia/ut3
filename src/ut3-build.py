#! /usr/bin/env python

#  This file is part of the Universal Template Translation Tools (UT3) project
#
#  Copyright (C) 2018 Helder Correia <@heldercorreia>
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; see the file COPYING.  If not, write to
#  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.

from optparse import OptionParser
from optparse import make_option
import os
import sys

from formats import *
from ut3 import support
from ut3.pofile import Ut3PoFile
from ut3.template import UT3Template

# -------------------------------------------------------------------------

def getLocalOptions():
    return [ \
       make_option("-t", "--templates", dest="templates", \
                   help="specify the template input tree path"), \
       make_option("-p", "--languages", dest="languages", \
                   help="specify the languages (PO) input tree path"), \
       make_option("-o", "--output", dest="output", \
                   help="specify the output tree path") \
    ]

# -------------------------------------------------------------------------

def findLanguagePaths(rootPath):
    result = []
    for root, _, files in os.walk(rootPath):
        for fileName in files:
            if fileName.endswith(".po"):
                result.append(os.path.join(root, fileName))
    return result

# -------------------------------------------------------------------------

def findTemplateRelativePaths(rootPath):
    oldDir = os.getcwd()
    os.chdir(rootPath)
    result = []
    for root, _, files in os.walk("."):
        for fileName in files:
            if not hasAllowedFileFormat(fileName):
                continue
            fullPath = os.path.join(root, fileName)
            result.append(fullPath[2:])
    os.chdir(oldDir)
    return result
# -------------------------------------------------------------------------

def buildStores(rootPath):
    result = dict()
    languagePaths = findLanguagePaths(rootPath)
    for path in languagePaths:
        if not path.endswith(".po"):
            continue
        languageCode = os.path.basename(path)[:-3]
        poFile = Ut3PoFile(path)
        store = poFile.parse()
        result[languageCode] = store
    return result

# -------------------------------------------------------------------------

def main():
    parser = OptionParser(usage="usage: %prog [options]", conflict_handler="resolve")
    parser.add_options(support.get_global_options())
    parser.add_options(getLocalOptions())
    (options, args) = parser.parse_args()
    totalArgs = len(args)

    if totalArgs > 0:
        support.print_warning_message("unused argument(s) %s" % (" ".join(args)))

    if (len(sys.argv) - totalArgs) == 1:
        parser.print_help()
        sys.exit()

    delimiters = support.extract_delimiters(options)

    support.exit_if(options.languages is None, "no language translation input tree path specified")
    support.exit_if(not os.path.isdir(options.languages), "specified language tree path is not a directory")
    support.exit_if(options.templates is None, "no template input tree path specified")
    support.exit_if(not os.path.isdir(options.templates), "specified template tree path is not a directory")
    support.exit_if(options.output is None, "no output tree path specified")
    support.exit_if(not os.path.isdir(options.output), "specified output tree path is not a directory")

    stores = buildStores(options.languages)
    support.exit_if(not stores, "could not find any language translation fle in specified {} directory".format(options.languages))
    for languageCode in stores:
        try:
            dirPath = os.path.join(options.output, languageCode)
            os.mkdir(dirPath)
        except FileExistsError:
            support.exit_if(not os.path.isdir(dirPath), "a file named \"{}\"" \
                " already exists in generated output directory, could not" \
                " create directory with the same name".format(languageCode))

    templates = findTemplateRelativePaths(options.templates)
    for templateRelativePath in templates:
        template = UT3Template(os.path.join(options.templates, templateRelativePath), delimiters)
        for languageCode in stores:
            templateOutputPath = os.path.join(options.output, languageCode, templateRelativePath)
            if options.verbose:
                print("Generating: {}".format(templateOutputPath))
            os.makedirs(os.path.dirname(templateOutputPath), exist_ok=True)
            template.convert(stores[languageCode], templateOutputPath)

    return 0

# -------------------------------------------------------------------------

if __name__ == "__main__":
    main()
