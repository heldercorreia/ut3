#! /usr/bin/env python

#  This file is part of the Universal Template Translation Tools (UT3) project
#
#  Copyright (C) 2007 Marco Wegner <ogre.crewman@googlemail.com>
#  Copyright (C) 2007, 2018 Helder Correia <@heldercorreia>
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; see the file COPYING.  If not, write to
#  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.

from optparse import OptionParser
from optparse import make_option
import sys

from ut3 import support
from ut3.pofile import Ut3PoFile
from ut3.template import UT3Template

# -------------------------------------------------------------------------

def getLocalOptions():
    return [ \
       make_option("-t", "--template", dest="template", help="specify the template file"), \
       make_option("-i", "--input", dest="infile", help="specify the input file"), \
       make_option("-o", "--output", dest="outfile", help="specify the output file") \
    ]

# -------------------------------------------------------------------------

def main():
    parser = OptionParser(usage="usage: %prog [options]", conflict_handler="resolve")
    parser.add_options(support.get_global_options())
    parser.add_options(getLocalOptions())

    (options, args) = parser.parse_args()

    totalArgs = len(args)

    if totalArgs > 0:
        support.print_warning_message("unused argument(s) %s" % (" ".join(args)))

    if (len(sys.argv) - totalArgs) == 1:
        parser.print_help()
        sys.exit()

    delimiters = support.extract_delimiters(options)

    if options.infile is None:
        support.print_error_message("no input file specified")
        sys.exit()

    poFile = Ut3PoFile(options.infile)
    store = poFile.parse()

    if options.verbose:
        print("Processing %s" % (options.infile))

    if options.template is None:
        support.print_error_message("no template file specified")
        sys.exit()

    outputFileName = "output"
    if options.outfile is not None:
        outputFileName = options.outfile

    template = UT3Template(options.template, delimiters)
    template.convert(store, options.outfile)

    if options.verbose:
        print("Generating %s from template %s" % (outputFileName, options.template))

    return 0

# -------------------------------------------------------------------------

if __name__ == "__main__":
    main()
