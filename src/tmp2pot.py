#! /usr/bin/env python

#  This file is part of the Universal Template Translation Tools (UT3) project
#
#  Copyright (C) 2007 Marco Wegner <ogre.crewman@googlemail.com>
#  Copyright (C) 2007, 2018 Helder Correia <@heldercorreia>
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; see the file COPYING.  If not, write to
#  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.

from optparse import make_option
from optparse import OptionParser
import os
import sys

from formats import *
from ut3 import support
from ut3.pofile import Ut3PoFile
from ut3.store import UT3Store
from ut3.template import UT3Template

# -------------------------------------------------------------------------

def getLocalOptions():
    return [
        make_option("-p", "--path", dest="inpath", help="specify input path for recursive scanning (turns template1 not required)"),
        make_option("-o", "--output", dest="outfile", help="specify the output file")
    ]

# -------------------------------------------------------------------------

def findPathsRecursively(rootPath):
    oldDir = os.getcwd()
    os.chdir(rootPath)
    result = []
    for root, _, files in os.walk("."):
        for fileName in files:
            if not hasAllowedFileFormat(fileName):
                continue
            fullPath = os.path.join(rootPath, root, fileName)
            result.append(fullPath)
    os.chdir(oldDir)
    return result

def main():
    usage = "usage: %prog [options] template1 [template2, ...]"
    parser = OptionParser(usage, conflict_handler="resolve")
    parser.add_options(support.get_global_options())
    parser.add_options(getLocalOptions())

    (options, args) = parser.parse_args()

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit()

    delimiters = support.extract_delimiters(options)

    if len(args) == 0 and options.inpath is None:
        support.print_error_message("no template(s) or input path specified")
        sys.exit()

    superStore = UT3Store()

    if options.inpath is not None:
        args += findPathsRecursively(options.inpath)

    globalCount = 0
    for templateFile in args:
        template = UT3Template(templateFile, delimiters)
        templateStore = template.parse()
        totalEntriesInStore = len(templateStore.get_all())
        globalCount = globalCount + totalEntriesInStore
        superStore.extend(templateStore)

        if options.verbose:
            print("Scanning %s : (%i messages found)" % (os.path.abspath(templateFile), totalEntriesInStore))

    if options.verbose:
        totalEntriesInStore = len(superStore.get_all())
        print("Removing %i duplicate strings" % (globalCount - totalEntriesInStore))
        print("Found %i unique strings" % (totalEntriesInStore))
        print("Writing POT catalog to %s" % (os.path.abspath(options.outfile)))

    poFile = Ut3PoFile(options.outfile)
    poFile.write(superStore, True)

    return 0

# -------------------------------------------------------------------------

if __name__ == "__main__":
    main()
